var ZPlat = ZPlat || {};

var lives = 3;

ZPlat.GameState = {

  init: function(level) {    
    this.currentLevel = level || 'level2';
    //constants
    this.RUNNING_SPEED = 180;
    this.JUMPING_SPEED = 400;
    this.BOUNCING_SPEED = 300;
    this.ACCELERATION = 180;
    this.JUMP_SPEED = 400;

    //gravity
    //this.game.physics.arcade.gravity.y = 1000; 

    
    //cursor keys to move the player
    //this.cursors = this.game.input.keyboard.createCursorKeys();

  },
  preload: function() {
    console.log("preloading the level!");

  },
  create: function() {
    //load current level with player, enemies, and assets
    this.loadLevel();

    //player health
    this.health = 5;
    this.score = 0;

    var healthText;
    var scoreText;
    this.facing = "right";

    this.healthText = this.game.add.text(16, 16, 'Health: ' + this.health, { fontSize: '16px', fill: '#000' });
    this.livesText = this.game.add.text(100, 16, 'Lives: ' + lives, { fontSize: '16px', fill: '#000'});
    this.scoreText = this.game.add.text(600, 16, 'Score: ' + this.score, { fontSize: '16px', fill: '#000' });
    this.healthText.fixedToCamera = true;
    this.livesText.fixedToCamera = true;
    this.scoreText.fixedToCamera = true;

    //show on-screen touch controls
    //this.createOnscreenControls();

    // Capture certain keys to prevent their default actions in the browser.
    // This is only necessary because this is an HTML5 game. Games on other
    // platforms may not need code like this.
    this.game.input.keyboard.addKeyCapture([
        Phaser.Keyboard.LEFT,
        Phaser.Keyboard.RIGHT,
        Phaser.Keyboard.UP,
        Phaser.Keyboard.DOWN,
        Phaser.Keyboard.SPACEBAR,
        Phaser.Keyboard.X
    ]);    

  },   
  update: function() {
    this.collisionLib();
    
    //generic platformer behavior (as in Monster Kong)
    this.player.body.velocity.x = 0;

    if (this.leftInputIsActive()) {
        this.facing = "left";
        // If the LEFT key is down, set the player velocity to move left
        this.player.body.velocity.x = -this.ACCELERATION;
        this.player.scale.setTo(-.52, .52);
        this.player.play('walking');
    } else if (this.rightInputIsActive()) {
        this.facing = "right";
        // If the RIGHT key is down, set the player velocity to move right
        this.player.body.velocity.x = this.ACCELERATION;
        this.player.scale.setTo(.52, .52);
        this.player.play('walking'); 
    } else if (this.upInputIsActive()) {
        this.player.play('jumping'); 
    } else {
      //player stay still and load frame 3 - stationary image
        this.player.body.velocity.x = 0;
        if(this.facing == "left") {
          this.player.scale.setTo(-.52, .52);
        } else {
          this.player.scale.setTo(.52, .52);
        }
        this.player.frame = 0;
    }
    if(this.spacebarInputIsActive()) {
      this.fireBullet();
    }
    


    // Set a variable that is true when the player is touching the ground
    var onTheGround = this.player.body.blocked.down;
    

    // If the player is touching the ground, let him have 2
    if (this.player.body.touching.down || this.player.body.blocked.down) {
        console.log(this.standing);
        this.jumps = 2;
        this.jumping = false;
    }

    // Jump!
    if (this.jumps > 0 && this.upInputIsActive(5)) {
        this.player.body.velocity.y = -this.JUMP_SPEED;
        this.jumping = true;
        console.log('jump');
        this.player.play('walking');
        this.jumpSound.play();
    }

    // Reduce the number of available jumps if the jump input is released
    if (this.jumping && this.upInputReleased()) {
        this.jumps--;
        this.jumping = false;
    }
    
    //kill enemy if it falls off
    if(this.player.bottom == this.game.world.height){
      this.health = this.health - 1;
      this.healthText.setText("Health: " + this.health);
      lives--;

      if (lives === 0) {
        this.gameOver();
        this.bgMusic.stop();
      } else {
        this.game.state.start('Game', true, false, this.currentLevel);
        this.bgMusic.stop();
      }
    }

    //check level for platform loading
    if (this.currentLevel == 'levelz') {
      this.platformsMovement();
    }
    
    this.standing = this.player.body.touching.down || this.player.body.blocked.down;
  },//end update

  collisionLib: function() {
    //collision between the player, enemies and the collision layer
    this.game.physics.arcade.collide(this.player, this.collisionLayer, this.collideLayer, null, this); 
    this.game.physics.arcade.collide(this.enemies, this.collisionLayer); 
    
    //collision between player and enemies
    this.game.physics.arcade.collide(this.player, this.enemies, this.hitEnemy, null, this);

    //collision between player and enemies
    this.game.physics.arcade.collide(this.player, this.enemies, this.hitEnemy, null, this);

    //collision between the player and the coins
    this.game.physics.arcade.overlap(this.player, this.coins, this.collectCoin, null, this);
    
    //overlap between player and goal
    this.game.physics.arcade.overlap(this.player, this.goal, this.changeLevel, null, this);

    //collision between bullets and enemies
    this.game.physics.arcade.collide(this.bullets, this.enemies, this.shootEnemy, null, this);

    //collision between player and platforms
    this.physics.arcade.collide(this.player, this.platforms, this.setFriction, null, this);
  },

  loadLevel: function(){

    //create a tilemap object
    this.map = this.add.tilemap(this.currentLevel);
    console.log(this.currentLevel);

    //join the tile images to the json data
    this.map.addTilesetImage('tiles_spritesheet', 'gameTiles');
    this.map.addTilesetImage('tiles_spritesheet2', 'gameTiles2');
    this.map.addTilesetImage('greenhillzone', 'backgroundTile');
    this.map.addTilesetImage('marioHills', 'marioBackground');

    //create tile layers
    this.collisionLayer = this.map.createLayer('collisionLayer');

    //coin sound
    this.coinSound = this.game.add.audio('coinSound');

    //jump sound
    this.jumpSound = this.game.add.audio('jumpSound');

    //enemy dies sound
    this.enemyDiesSound = this.game.add.audio('enemyDiesSound');
    
    //game music
    this.bgMusic = this.game.add.audio('bgMusic');
    this.bgMusic.play();
    
    //collision layer should be collisionLayer
    this.map.setCollisionBetween(1, 300, true, 'collisionLayer');
    
    //resize the world to fit the layer
    this.collisionLayer.resizeWorld();

    //send background to the back
    this.loadBackground(this.currentLevel);
    this.game.world.sendToBack(this.background);
    
    //create the goal
    var goalArr = this.findObjectsByType('goal', this.map, 'objectsLayer');
    this.goal = this.add.sprite(goalArr[0].x, goalArr[0].y, goalArr[0].properties.key);
    this.game.physics.arcade.enable(this.goal);
    this.goal.body.allowGravity = false;
    this.goal.nextLevel = goalArr[0].properties.nextLevel;
    
    //create player
    var playerArr = this.findObjectsByType('player', this.map, 'objectsLayer');
    this.player = this.add.sprite(playerArr[0].x, playerArr[0].y, 'player', 3);
    this.player.anchor.setTo(0.5);
    this.player.animations.add('walking', [1, 2, 3, 4, 5], 8, true);
    this.player.animations.add('jumping', [8], 8, true);
    this.player.animations.add('walling', [7], 8, true);
    this.game.physics.arcade.enable(this.player);
    //or set physics:
    //this.game.physics.enable(this.player, Phaser.Physics.ARCADE);
    this.player.customParams = {};
    this.player.body.collideWorldBounds = true;
    this.player.body.gravity.y = 1000;

    //create bullets
    this.bullets = this.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicalBodyType = Phaser.Physics.Arcade;
    this.bullets.createMultiple(1, 'bullet');
    this.bullets.setAll('anchor.x', 0.8);
    this.bullets.setAll('anchor.y', 2);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);

    //create explosions
    this.explosions = this.add.group();
    this.explosions.createMultiple(30, 'kaboom');
    //this.explosions.forEach(setupInvader, this);

    //follow player with the camera
    this.game.camera.follow(this.player);
    
    //create enemies
    this.enemies = this.add.group();
    this.createEnemies();
    this.createCoins();

    
    //load in platforms on levelz
    if (this.currentLevel == 'levelz') {
      this.createPlatforms();
    };
    
  },

  createPlatforms: function(){
    this.platforms = this.add.physicsGroup();

    this.platforms.create(0, 64, 'ice-platform');
    this.platforms.create(200, 180, 'platform');
    this.platforms.create(400, 296, 'ice-platform');
    this.platforms.create(-160, 810, 'platform');

    this.platforms.setAll('body.allowGravity', false);
    this.platforms.setAll('body.immovable', true);
    this.platforms.setAll('body.velocity.x', 100);
  },

  platformsMovement: function(){
    this.platforms.forEach(this.wrapPlatform, this);
  },
  
  wrapPlatform: function(platform){
    platformLevelWidth = this.game.world.width;
    if (platform.body.velocity.x < 0 && platform.x <= -160) {
      platform.x = platformLevelWidth;
    } else if (platform.body.velocity.x > 0 && platform.x >= platformLevelWidth) {
      platform.x = -160;
    }
  },

  setFriction: function(player, platform) {
    if(platform.key === 'ice-platform') {
      player.body.x -= platform.body.x - platform.body.prev.x;
    }
  },

  loadBackground: function(level){
    switch(level) {
      case 'level2':
        this.background = this.game.add.sprite(0,0, 'level2Backgroundb');
        break;
      case 'levelz':
        this.background = this.game.add.sprite(0,0, 'background3');
        break;
      case 'level3':
        this.background = this.game.add.sprite(0,0, 'background3');
        break;
    }
    this.background.width = this.game.world.width;
    this.background.height = this.game.world.height;
  },

  createOnscreenControls: function(){
    this.leftArrow = this.add.button(20, this.game.height - 60, 'arrowButton');
    this.rightArrow = this.add.button(110, this.game.height - 60, 'arrowButton');
    this.actionButton = this.add.button(this.game.width - 100, this.game.height - 60, 'actionButton');

    this.leftArrow.alpha = 0.5;
    this.rightArrow.alpha = 0.5;
    this.actionButton.alpha = 0.5;

    this.leftArrow.fixedToCamera = true;
    this.rightArrow.fixedToCamera = true;
    this.actionButton.fixedToCamera = true;

    this.actionButton.events.onInputDown.add(function(){
      this.player.customParams.mustJump = true;
    }, this);

    this.actionButton.events.onInputUp.add(function(){
      this.player.customParams.mustJump = false;
    }, this);

    //left
    this.leftArrow.events.onInputDown.add(function(){
      this.player.customParams.isMovingLeft = true;
    }, this);

    this.leftArrow.events.onInputUp.add(function(){
      this.player.customParams.isMovingLeft = false;
    }, this);

    this.leftArrow.events.onInputOver.add(function(){
      this.player.customParams.isMovingLeft = true;
    }, this);

    this.leftArrow.events.onInputOut.add(function(){
      this.player.customParams.isMovingLeft = false;
    }, this);

    //right
    this.rightArrow.events.onInputDown.add(function(){
      this.player.customParams.isMovingRight = true;
    }, this);

    this.rightArrow.events.onInputUp.add(function(){
      this.player.customParams.isMovingRight = false;
    }, this);

    this.rightArrow.events.onInputOver.add(function(){
      this.player.customParams.isMovingRight = true;
    }, this);

    this.rightArrow.events.onInputOut.add(function(){
      this.player.customParams.isMovingRight = false;
    }, this);
  },
  findObjectsByType: function(targetType, tilemap, layer){
    var result = [];
    
    tilemap.objects[layer].forEach(function(element){
      if(element.properties.type == targetType) {
        element.y -= tilemap.tileHeight;        
        result.push(element);
      }
    }, this);
    
    return result;
  },
  changeLevel: function(player, goal){
    if (this.currentLevel == 'level3') {
      console.log("boom baby");
      this.game.state.start('EndScreen');
      //start and restart music
      this.bgMusic.stop();
    } else {
      this.game.state.start('Game', true, false, goal.nextLevel);
      //start and restart music
      this.bgMusic.stop();
    }
  },

  //create a sprite from an object
  createFromTiledObject: function(element, group) {
 
    var sprite = group.create(element.x, element.y, element.properties.sprite);
 
      //copy all properties to the sprite
      Object.keys(element.properties).forEach(function(key){
        sprite[key] = element.properties[key];
      });
 
  },
  createEnemies: function(){
    var enemyArr = this.findObjectsByType('enemy', this.map, 'objectsLayer');
    var enemyArr2 = this.findObjectsByType('enemy2', this.map, 'objectsLayer');
    var enemyArr3 = this.findObjectsByType('enemy3', this.map, 'objectsLayer');
    var enemySpikeArr = this.findObjectsByType('enemySpike', this.map, 'objectsLayer');
    var enemySpikeTopArr = this.findObjectsByType('enemySpikeTop', this.map, 'objectsLayer');
    var enemy, enemy2, enemy3, enemySpike, enemySpikeTop;
    
    enemyArr.forEach(function(element){
      enemy = new ZPlat.Enemy(this.game, element.x, element.y, 'slime', +element.properties.velocity, this.map);
      this.enemies.add(enemy);
    }, this);

    enemyArr2.forEach(function(element){
      enemy2 = new ZPlat.Enemy(this.game, element.x, element.y, 'slime2', +element.properties.velocity, this.map);
      this.enemies.add(enemy2);
    }, this);

    enemyArr3.forEach(function(element){
      enemy3 = new ZPlat.Enemy(this.game, element.x, element.y, 'fly', 1, this.map);
      this.enemies.add(enemy3);
    }, this);

    enemySpikeArr.forEach(function(element){
      enemySpike = new ZPlat.Enemy(this.game, element.x, element.y, 'enemySpike', 1, this.map);
      this.enemies.add(enemySpike);
    }, this);

    enemyArr3.forEach(function(element){
      enemySpikeTop = new ZPlat.Enemy(this.game, element.x, element.y, 'enemySpikeTop', 1, this.map);
      this.enemies.add(enemySpikeTop);
    }, this);
  },

  hitEnemy: function(player, enemy){
    if(enemy.body.touching.up){
      enemy.kill();
      this.enemyDiesSound.play();

      console.log("added to points!");
      this.score += 10;
      this.scoreText.setText("Score: " + this.score);
      player.body.velocity.y = -this.BOUNCING_SPEED;
    }
    else {
      this.health = this.health - 1;
      this.healthText.setText("Health: " + this.health);
      //this.createEnemies();
      //this.player.reset(32, 32);
      enemy.kill();
      if (this.health === 0) {
        lives--;
        if (lives === 0) {
          this.gameOver(); 
          this.bgMusic.stop(); 
        } else {
          this.game.state.start('Game', true, false, this.currentLevel);
          this.bgMusic.stop();
        }
        
      }
    }
  },
  shootEnemy: function(bullet, enemy){
    console.log("nailed ir!");
    this.score += 10;
    this.scoreText.setText("Score: " + this.score);
    enemy.kill();
    bullet.kill();
      //from here we can either add to score, make a sound, do whatever
  },
  createCoins: function(){

    this.coins = this.game.add.group();
 
    this.coins.enableBody = true;

    var coinArr = this.findObjectsByType('coin', this.map, 'objectsLayer');

    coinArr.forEach(function(element){
 
      this.createFromTiledObject(element, this.coins);
 
    }, this);
  },
  collectCoin: function(player, collectable){
    //play the sound
    this.coinSound.play();

    //add to the score
    this.score += 10;
    this.scoreText.setText("Score: " + this.score);
    
    //remove the coin
    collectable.destroy();
  },

  fireBullet: function () {

      //  To avoid them being allowed to fire too fast we set a time limit
      //if (this.game.time.now > bulletTime)
      //{
          //  Grab the first bullet we can from the pool
          this.bullet = this.bullets.getFirstExists(false);

          if (this.bullet)
          {
              //  And fire it
              this.bullet.reset(this.player.x, this.player.y + 8);
              if (this.facing == "left") {
                this.bullet.body.velocity.x = -400;
              } else {
                  this.bullet.body.velocity.x = 400;
              }
              
              bulletTime = this.game.time.now + 10;
          }
//      }

  },

  resetBullet: function(bullet) {
      //  Called if the bullet goes out of the screen
      this.bullet.kill();

  },
  gameOver: function(){
    lives = 3;
    this.health = 5;
    this.game.state.start('TitleScreen');
    this.bgMusic.stop();

  },
  collideLayer: function(){
    if (this.player.body.blocked.left || this.player.body.blocked.right){
      this.player.body.velocity.y = 100;
      this.jumps = 2; 
      console.log('walling');
      this.player.play('walling'); 
    }
    
  },

  leftInputIsActive: function() {
    var isActive = false;

    isActive = this.input.keyboard.isDown(Phaser.Keyboard.LEFT);
    isActive |= (this.game.input.activePointer.isDown && this.game.input.activePointer.x < this.game.width/4);

    return isActive;
  },

// This function should return true when the player activates the "go right" control
// In this case, either holding the right arrow or tapping or clicking on the right
// side of the screen.
rightInputIsActive: function() {
    var isActive = false;

    isActive = this.input.keyboard.isDown(Phaser.Keyboard.RIGHT);
    isActive |= (this.game.input.activePointer.isDown &&
        this.game.input.activePointer.x > this.game.width/2 + this.game.width/4);

    return isActive;
},

spacebarInputIsActive: function() {
  var isActive = false;
  isActive = this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR);
  isActive |= (this.game.input.activePointer.isDown &&
    this.game.input.activePointer.x > this.game.width/2 + this.game.width/4);

  return isActive;
},

// This function should return true when the player activates the "jump" control
// In this case, either holding the up arrow or tapping or clicking on the center
// part of the screen.
upInputIsActive: function(duration) {
    var isActive = false;
    isActive = this.input.keyboard.downDuration(Phaser.Keyboard.X, duration);
    isActive |= (this.game.input.activePointer.justPressed(duration + 1000/60) &&
        this.game.input.activePointer.x > this.game.width/4 &&
        this.game.input.activePointer.x < this.game.width/2 + this.game.width/4);
    return isActive;
},

// This function returns true when the player releases the "jump" control
upInputReleased: function() {
    var released = false;

    released = this.input.keyboard.upDuration(Phaser.Keyboard.X);
    released |= this.game.input.activePointer.justReleased();

    return released;
}
  
};
