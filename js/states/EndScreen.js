var ZPlat = ZPlat || {};

//setting game configuration and loading the assets for the loading screen
ZPlat.EndScreenState = {
  init: function() { 
    
    //scaling options
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    
    //have the game centered horizontally
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;

    //physics system
    this.game.physics.startSystem(Phaser.Physics.ARCADE);    
  },
  preload: function() {
    //assets we'll use in the loading screen
    this.game.load.image('end', 'assets/images/end.png');
    this.game.load.image('background', 'assets/images/starfield.png');
  },
  create: function() {
    this.game.input.keyboard.addKeyCapture([
        Phaser.Keyboard.ENTER
    ]);
    this.background = this.game.add.sprite(0, 0, 'background');
    this.background.width = this.game.width;  
    this.background.height = this.game.height;  
    this.game.add.sprite(-25, this.game.height/3, 'end');

  },
  update: function() {
    if (this.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
      this.state.start('TitleScreen');
    }

  }
};