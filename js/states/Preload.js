var ZPlat = ZPlat || {};
var bullets;
var bulletTime = 0;
var cursors;
var fireButton;
var explosions;
//loading the game assets
ZPlat.PreloadState = {
  preload: function() {
    //show loading screen
    this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloadbar');
    this.preloadBar.anchor.setTo(0.5);
    this.preloadBar.scale.setTo(3);

    this.load.setPreloadSprite(this.preloadBar);

    console.log("in preload");

//load game assets    
    this.load.image('platform', 'assets/images/platform.png');
    this.load.image('goal', 'assets/images/goal.png');

//enemies
    this.load.image('slime', 'assets/images/slime.png');
    this.load.image('slime2', 'assets/images/slime2.png');
    this.load.image('enemySpike', 'assets/images/enemySpike.png');
    this.load.image('enemySpikeTop', 'assets/images/enemySpikeTop.png');
    this.load.image('fly', 'assets/images/fly_spritesheet.png');

//coins
    this.load.image('goldCoin', 'assets/images/goldCoin.png');
    this.load.audio('coinSound', 'assets/audio/coin.mp3');

//audio
    this.load.audio('bgMusic', 'assets/audio/bg-music.mp3'); 
    this.load.audio('jumpSound', 'assets/audio/Jump_01.mp3'); 
    this.load.audio('enemyDiesSound', 'assets/audio/Explosion_03.mp3');    

//images for shooting and explosions
    this.load.image('bullet', 'assets/images/bullet.png');
    this.load.image('enemyBullet', 'assets/images/enemy-bullet.png');
    this.load.spritesheet('invader', 'assets/images/invader32x32x4.png', 32, 32);
    this.load.image('ship', 'assets/images/player.png');
    this.load.spritesheet('kaboom', 'assets/images/explode.png', 128, 128);
    this.load.image('starfield', 'assets/images/starfield.png');

//backgrounds
    this.load.image('background2', 'assets/images/background2.png');
    this.load.image('background3', 'assets/images/background3.png');
    this.load.image('level2Background', 'assets/images/level2background.png');
    this.load.image('level2Backgroundb', 'assets/images/level2backgroundb.png');

//platforms
    this.load.image('platform', 'assets/images/platform.png');
    this.load.image('ice-platform', 'assets/images/ice-platform.png');

//this.load.spritesheet('player', 'assets/images/player_spritesheet.png', 28, 30, 5, 1, 1);
    this.load.spritesheet('player', 'assets/images/sprite-scott3.png', 64, 64, 9, 1, 1); 
    this.load.spritesheet('fly', 'assets/images/fly_spritesheet.png', 35, 18, 2, 1, 2);   

    this.load.image('arrowButton', 'assets/images/arrowButton.png');    
    this.load.image('actionButton', 'assets/images/actionButton.png'); 
    
    this.load.image('gameTiles', 'assets/images/tiles_spritesheet.png');
    this.load.image('gameTiles2', 'assets/images/tiles_spritesheet2.png');     
    this.load.image('backgroundTile', 'assets/images/sonicbackgroundf.png'); 
    this.load.image('marioBackground', 'assets/images/mariohillsbackground.png');

//JSON data    
    this.load.tilemap('level2', 'assets/levels/level2.json', null, Phaser.Tilemap.TILED_JSON);
    this.load.tilemap('levelz', 'assets/levels/levelz.json', null, Phaser.Tilemap.TILED_JSON);
    this.load.tilemap('level5', 'assets/levels/level5.json', null, Phaser.Tilemap.TILED_JSON);
    this.load.tilemap('level3', 'assets/levels/level3.json', null, Phaser.Tilemap.TILED_JSON);
  },

  
  create: function() {
    console.log("got hgere");
    this.state.start('TitleScreen');
  }
};